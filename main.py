#!/usr/bin/env python3

import os

from fastapi import FastAPI
import uvicorn

from autohelp.fastapi import simplify_operation_ids


# https://fastapi.tiangolo.com/tutorial/metadata/
app = FastAPI(
    title='example-service-python',
    version='0.0.1',
)


@app.get('/')
def get_root():
    """Say hello."""
    if 'GREETING' in os.environ:
        return os.environ['GREETING']
    else:
        return 'Hello!'


@app.get('/healthz', include_in_schema=False)
def get_healthz():
    """Kubernetes health probe."""
    return 'OK'


# Use function names as operation IDs in the OpenAPI schema.
simplify_operation_ids(app)

if __name__ == '__main__':
    # DEVELOPMENT is used by some autohelp.fastapi dependencies.
    os.environ['DEVELOPMENT'] = 'true'
    uvicorn.run(
        'main:app',
        port=8080,
        reload=True,
        log_config='log_config.json')
