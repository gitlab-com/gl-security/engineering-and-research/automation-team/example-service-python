## Build Image
## https://pipenv.pypa.io/en/stable/docker/

# See .python-version
FROM python:3.11 AS builder

ENV PATH=/root/.local/bin:$PATH
ENV PIPENV_VENV_IN_PROJECT=1

WORKDIR /app

# Create virtual environment
RUN pip install -qq --user pipenv
COPY Pipfile Pipfile.lock ./
RUN $HOME/.local/bin/pipenv sync

## Runtime Image

# See .python-version
FROM python:3.11 AS runtime

ENV PATH=/app/.venv/bin:$PATH

WORKDIR /app
RUN adduser --disabled-password --gecos "" app && chown app:app .

# Copy virtual environment from builder
COPY --chown=app:app --from=builder /app/.venv/ .venv/

# Copy application files
COPY --chown=app:app *.json *.py *.yaml *.yml ./

USER app
CMD ["gunicorn", "main:app"]
