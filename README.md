# example-service-python

SecAuto example service in Python, using [FastAPI][].

To create your own service, download the [source code][] of this example and
add it to a new project.

The [GitLab CI][] configuration in *.gitlab-ci.yml* defines a pipeline that
builds the Docker image and triggers the deployment pipeline in the [deployment
project][].

Please make sure to change `DEPLOYMENT_PROJECT` in *.gitlab-ci.yml* for your
service!

[deployment project]: https://gitlab.com/gitlab-private/gl-security/engineering-and-research/automation-team/kubernetes/secauto/example-service-python
[fastapi]: https://fastapi.tiangolo.com/
[gitlab ci]: https://docs.gitlab.com/ee/ci/
[source code]: https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/example-service-python/-/archive/main/example-service-python-main.tar.gz

## Development

Dependencies are handled by [Pipenv][], unit tests by [pytest][]. In local
development, API documentation is available at <http://127.0.0.1:8080/docs>.

Local development:

```bash
# Create .env file
cp sample.env .env
# Activate virtual environment
pipenv shell
# Install dependencies, including development packages
pipenv sync --dev
# Update dependencies
pipenv update
# Run tests
pytest
# Check code style
pycodestyle .
# Run service
./main.py
```

Docker image:

```bash
# Build Docker image
docker build -t example .
# Run service
docker run -it --env-file .env -p 8080:8080 example
```

[pipenv]: https://pipenv.pypa.io/
[pytest]: https://docs.pytest.org/
