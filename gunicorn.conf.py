import os


LOG_FORMAT = os.environ.get('LOG_FORMAT', '[%(process)d] [%(levelname)s] %(message)s')  # noqa
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')

bind = "0.0.0.0:8080"

worker_class = "uvicorn.workers.UvicornWorker"
threads = 4

logconfig_dict = {
    'version': 1,
    'formatters': {
        'default': {
            'format': LOG_FORMAT,
        },
        'json': {
            '()': 'logging_json.JSONFormatter',
            'fields': {
                'process': 'process',
                'severity': 'levelname',
            }
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stdout',
        },
        'json': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
            'stream': 'ext://sys.stdout',
        },
    },
    'root': {
        'handlers': ['json'],
        'level': LOG_LEVEL,
    },
    'loggers': {
        'gunicorn.access': {
            'handlers': ['console'],
        },
        'gunicorn.error': {
            'handlers': ['console'],
        },
    },
}
